// Implement web-server which hosts and serves files
// Write every request info to logs;
// Application should support log, txt, json, yaml, xml, js file extensions ( consider filename may contain '.' symbol)
// - POST / createFile
// GET files / getFiles
// GET filename / getFile
// all checks:
// 1.wrong request
// 2.wrong file type
// 3.file not found
// Response as object with message
const fs = require('fs');
const express = require('express');
const app = express();
const port = 8080;
const bodyParser = require('body-parser')
const multer = require('multer'); // v1.0.5
const { error } = require('console');
const { match } = require('assert');
const upload = multer() // for parsing multipart/form-data

function log(req) {
  console.log((new Date(Date.now()).toLocaleString()), req.method, req.url);
}

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

var validFileName = (function () {
  var rg1 = /^[^\\/:\*\?"<>\|]+(.log|.txt|.yaml|.xml|.js)$/; // forbidden characters \ / : * ? " < > |
  var rg2 = /^\./; // cannot start with dot (.)
  var rg3 = /^(nul|prn|con|lpt[0-9]|com[0-9])(\.|$)/i; // forbidden file names
  return function isValid(fname) {
    return rg1.test(fname) && !rg2.test(fname) && !rg3.test(fname);
  }
})();

app.post('/api/files/', upload.array(), (req, res, next) => {
  log(req)
  try {
    if (!req.body.filename) throw 'filename';
    if (!validFileName(req.body.filename)) throw 'filename';
    if (!req.body.content) throw 'content';
    fs.writeFile('./files/' + req.body.filename, req.body.content, (err) => {
      if (err) throw err;
      console.log('success');
      res.status(200).send({ "message": 'File created successfully' });//, 'POST File:'+req.body.filename+' ' +req.body.content);    //console.log({});
    }
    );
  } catch (e) {
    if (e === 'filename') {
      res.status(400).send({ "message": 'Please specify valid filename' });
      console.error('Bad ' + e);
    } else if (e === 'content') {
      res.status(400).send({ "message": "Please specify 'content' parameter" });
      console.error('Bad ' + e);
    } else {
      res.status(500).send({ "message": 'Server error' });
      console.error(e);
    }
  }
});
app.get('/api/files/', (req, res) => {
  log(req)
  try {
    fs.readdir('./files/', 'utf8', (err, data) => {
      if (err) throw err;
      res.status(200).send({
        "message": 'Success',
        "files": data
      });
    });
  } catch (e) {
    res.status(500).send({ "message": 'Server error' });
    console.error(e);
  }
});

app.get('/api/files/:filename', (req, res) => {
  log(req)
  if (!req.params.filename) throw 'filename';
  if (!validFileName(req.params.filename)) throw 'filename';
  fs.stat("./files/" + req.params.filename, (err, stat) => {
    try {
      if (err) throw err 
      else {
        fs.readFile('./files/' + req.params.filename, 'utf8', (err, data) => {
          if (err) return err 
           res.status(200).send({
             "message": 'Success',
             "filename": req.params.filename,
             "content": data,
             "extension": req.params.filename.slice((req.params.filename.lastIndexOf(".") - 1 >>> 0) + 2),
             "uploadedDate": stat.ctime
           });
           console.log('Success');
        });
      }
    } catch (e) {
      if (e.code === 'ENOENT') {
        res.status(400).send({ "message": `No file with ${req.params.filename} filename found` });
        console.error(e);
      } else {
        res.status(500).send({ "message": 'Server error' });
        console.error(e);
      }
    }
  });
});

app.use(function (req, res, next) {
  log(req)
  res.status(400).send({ "message": 'Client error' });
  next();
});

app.listen(port, () => {
  console.log(`My Server is listening on port ${port}`);
})