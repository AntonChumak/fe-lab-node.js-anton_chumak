const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    userId:{
        type:String,
        required:true,
    },
    completed:{
        type:Boolean,
        //required:true
    },
    text:{
        type:String,
        required:true
    },
    createdDate:{
        type:Date,
        default: Date.now
    }
});

module.exports = mongoose.model('note', NoteSchema);