/* eslint-disable quotes */
/* eslint-disable require-jsdoc */

// 1. Ability to register users;
// 2. User is able to login into the system;
// 3. User is able to view his profile info;
// 4. User is able to view only personal notes,
//      * provide pagination parameters
//      * for notes list, request note by id;
// 5. User is able to
//      * add,
//      * delete personal notes;
// 6. User can check/uncheck any note;
// 7. User can manage (5?) notes and personal profile only with valid
//    JWT token in request;

const express = require('express');
const app = express();
const port = 8080;

const {MongoClient, ServerApiVersion} = require('mongodb');
const uri = "mongodb+srv://root:<password>@fe-lab-chumak.7h8dl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
// eslint-disable-next-line max-len
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
client.connect(err => {
  const collection = client.db("test").collection("devices");
  // perform actions on the collection object
  client.close();
});

/** Logs a request
 * @arg {obj} req - request to log
*/
function log(req) {
  console.log((new Date(Date.now()).toLocaleString()), req.method, req.url);
}

/** Error model
 * @param {str} message - request to log
 *
*/
class Error {
  constructor(message = 'Error message') {
    this.message = message;
  };
};

/** Logs a request
 * @param {obj} req - request to log
*/
class Credentials {
  constructor(username = 'Kyrylo', password = 've518dl3') {
    this.username = username;
    this.password = password;
  };
};

/** Logs a request
 * @arg {obj} req - request to log
*/
class User {
  constructor(_id = '5099803df3f4948bd2f98391',
      username = 'Kyrylo',
      createdDate = '2020-10-28T08:03:19.814Z') {
    this._id = _id;
    this.username = username;
    this.createdDate = createdDate;
  };
};

/** Logs a request
 * @arg {obj} req - request to log
*/
class Note {
  constructor(_id = '5099803df3f4948bd2f98391',
      userId = '5099803df3f4948bd2f98391',
      completed = false,
      text = 'Complete second homework',
      createdDate = '2020-10-28T08:03:19.814Z') {
    this._id = _id;
    this.userId = userId;
    this.completed = completed;
    this.text = text;
    this.createdDate = createdDate;
  };
};

let error = new Error;
let credentials = new Credentials;
let user = new User;
let note = new Note;

// Users
/*
app.get( '/api/users/me', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});
app.delete( '/api/users/me', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});
app.patch( '/api/users/me', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});

// Notes

app.get( '/api/notes', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});
app.post( '/api/notes', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});
app.get( '/api/notes/:id', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});
app.put( '/api/notes/:id', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});
app.patch( '/api/notes/:id', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});
app.delete( '/api/notes/:id', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});
*/
// Auth

app.post( '/api/auth/register', (req, res) => {
  log(req);
  console.log(req.user);
  res.status(200).send({'message': 'OK'});
});
app.post( '/api/auth/login', (req, res) => {
  log(req);
  // res.status(400).send({'message': 'Client error'});
});

// General

app.use( function(req, res, next) {
  log(req);
  res.status(400).send({'message': 'Client error'});
  next();
});

app.listen(port, () => {
  console.log(`My Server is listening on port ${port}`);
});
