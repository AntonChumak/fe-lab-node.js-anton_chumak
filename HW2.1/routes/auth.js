const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('config');
const auth = require('../middleware/auth');

const User = require('../models/User');

// export let logedInUserToken;

/**
 * @route    POST api/auth/register
 * @desc     User Registration
 * @access   Public
**/
router.post('/register', async (req, res) => {
  const {username, password} = req.body;
  try {
    let user = await User.findOne({username});
    if (user) {
      return res.status(400).json({msg: 'User already exist'});
    }
    user = new User({
      username,
      password});

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);
    await user.save();

    const payload = {user: {id: user.id}};
    jwt.sign(payload, config.get('jwtSecret'), {
      expiresIn: 360000},
    (err, token) => {
      if (err) throw err;
      res.json({'message': 'Sucsess'});
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

/** @route    POST api/auth/login
 * @desc     Auth user and get token(login)
 * @access   Public
 * */
router.post('/login',
    async (req, res) => {
      const {username, password} = req.body;
      try {
        const user = await User.findOne({username});
        if (!user) {
          return res.status(400).json({msg: 'Invalid credentials'});
        }
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
          return res.status(500).json({msg: 'Password invalid'});
        }
        const payload = {user: {id: user.id}};
        jwt.sign(payload, config.get('jwtSecret'),
            {expiresIn: 360000},
            (err, token) => {
              if (err) throw err;
              res.json({
                'message': 'Sucsess',
                'jwt_token': token});
            });
      } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
      };
    });

module.exports = router;
