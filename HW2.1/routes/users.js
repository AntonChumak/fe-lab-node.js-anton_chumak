const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('config');
const auth = require('../middleware/auth')

const User = require('../models/User');

//@route    GET api/users
//@desc     Get logged in user profile
//@access   Private
router.get('/me', auth, async (req,res)=>{
  try {
      //console.log(req.user);
      //Get user details except the password
      const user = await User.findById(req.user.id).select('-password -__v');  
      res.json(user); 
  } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
  }
});
//@route    DELETE api/users
//@desc     Delete logged in user
//@access   Private
router.delete('/me', auth, async (req,res)=>{  
    try {
        await User.deleteOne({_id:req.user.id});  
//        await user.save();
        res.json({
        "message": "Success"
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});
//@route    PATCH api/users
//@desc     Change logged in user password
//@access   Private
router.patch('/me', auth, async (req,res)=>{
    const { oldPassword, newPassword } = req.body;
    const user = await User.findById(req.user.id).select('-__v');  
    try {
        if(!bcrypt.compare(oldPassword,user.password)){
            return res.status(400).json({msg:'Invalid old password'});
        } 
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(newPassword,salt);
        await user.save();
        const payload = {
            user:{
                id:user.id
            }
        }
        jwt.sign(payload,config.get('jwtSecret'),
            {
                expiresIn:360000
            },
            (err)=>{
                if(err) throw err;
                res.json({
                    "message": "Success"
                });
            }
        )
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});

module.exports = router;