const jwt = require('jsonwebtoken');
const config = require('config');
//import {logedInUserToken} from "./auth";

module.exports = function(req,res,next){      
    //Get token from request header
    const authorization = req.headers.authorization;
    const token = authorization.split(' ')[1];
 //   const token = req.header('jwt_token');
    //const token = logedInUserToken;
    if(!token){
        return res.status(401).json({msg:'Token not found, authorization failed'});
    }

    try {
        //verify token & extract payload
        const decoded = jwt.verify(token,config.get('jwtSecret'));  
        req.user = decoded.user;
        next();
    } catch (err) {
        res.status(401).json({msg: 'Token is invalid'});
    }
}